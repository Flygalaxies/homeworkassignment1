/*
*   Homework Assignment 1
*   Entry Point to the API
*/

let http = require('http');
let url = require('url');
let StringDecoder = require('string_decoder').StringDecoder;

//Create the server
let httpServer = http.createServer((req, res) => {
    
    // Get the http data
    let path = url.parse(req.url, true).pathname.replace(/^\/+|\/+$/g, '');    
    let queryStringObject = url.parse(req.url, true).query;    
    let method = req.method.toLowerCase();    
    let headers = req.headers;
    
    //Get the payload, if any
    let decoder = new StringDecoder('utf-8');
    let buffer = '';
    req.on('data', (data) => {
        buffer += decoder.write(data);
    })
    req.on('end', () => {
        buffer += decoder.end();
        
        //Choose specific handler, default to not found
        let chosenHandler = typeof (router[path]) !== 'undefined' ? router[path] : handlers.notFound;    

        let data = {
            'path': path,
            'queryStringObject': queryStringObject,
            'method': method,
            'headers': headers,
            'payload': buffer,            
        }

        //route handlers request to handler specified
        chosenHandler(data, (statusCode, payload) => {
            //use the status code called back by the handler or defualt to 200
            statusCode = typeof (statusCode) == 'number' ? statusCode : 200;
            //use the payload called back by the handler or default to empty obj
            payload = typeof (payload) == 'object' ? payload : {};
            //convert the payload to a string
            let payloadString = JSON.stringify(payload);
            //return response
            res.setHeader('Content-Type', 'application/json')
            res.writeHead(statusCode);
            res.end(payloadString);                        
        })
    })
})

//Listen To port 4200
httpServer.listen(4200, () => {
    console.log("Listening on Port: 4200");
})


handlers = {};
handlers.hello = (data, callback) => {           
    callback(200, {'Message': "Why Hello there fair mystical creature!"});
}
handlers.notFound = (data, callback) => {
    callback(404);
}

//route to specific handlers based on path
let router = {
    "hello": handlers.hello
}